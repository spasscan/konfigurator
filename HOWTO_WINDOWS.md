Introduction
============

* the following steps are testet in a new Windows instance
* the steps are not the only way for start and setup konfigurator
* you are safe handling skill in GIT, Python3, Virtualenv ... - use your own way 

GIT
===

* first, we need a git client.
* e.g. https://git-scm.com/download/win
* install it and use the defaults. except you know what you do

* create a folder for spasscan stuff.
* got to this folder via explorer. in the context menu (right mouse button) you should see 'git gui here' - click it.
* choose 'clone existing repository'
* HERE SCREENSHOT
* Done. Now you have the program files in the subfolder 'konfigurator'

Python3
=======

* https://www.python.org/downloads/
* install your version python 3.X
* test the version via CMD.exe
* HERE SCREENSHOT
* if you see python 3.X Version in the first line - everything is allright


Virtual ENV
===========

* open CMD.exe
* 'pip install virtualenv'
* 'pip install virtualenvwrapper-win'
* test the virtualenv mechanics
* 'mkvirtualenv my_virtual_env'
* now you should see evrey commandline starts with '(my_virtual_env)'
* to remove this env type 'rmvirtualenv my_virtual_env -r'

Konfigurator Setup
==================

* open CMD.exe
* go to folder where git clone is (see GIT above)
* activate virtualenv by 'mkvirtualenv ENVNAME'. ENVNAME= free name e.g. spasscan
* now setup the konfigurator via 'pip install .' important note: don't forget the point '.'
* start the konfigurator: 'konfigurator_tk'

Konfigurator Upgrade
====================

* via File Explorer: goto the folder were konfigurator is cloned
* choose 'Git Bash Here' on the konfigurator context menu
* you should have comandline prompt on the right position
* just type 'git pull'
* now use the CMD.exe
* goto the konfigurator folder
* ensure the virtualenv 'mkvirtualenv ENVNAME'
* upgrade: 'pip install --upgrade .'  
