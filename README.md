Introduction
============

small configurator for SpassCAN Modules

see also

* [SB Dekoder](http://sbdekoder.de)

* [SpassBahn Forum - SpassCan](http://www.spassbahn.de/forum/index.php?thread/10384-projekt-spa%C3%9Fcan-r%C3%BCckmeldung-auf-unserer-anlage/)

* [SpassBahn Forum - Konfigurator]( )


Installation by virtualenv
==========================

Linux e.g. Ubuntu, Debian
-------------------------

1. install git, python3, virtualenv
```
sudo apt-get install python3 virtualenv git
```
2. clone the repository (download source)
```
git clone https://gitlab.com/spasscan/konfigurator.git
cd konfigurator
```
3. create virtualenv, ENVNAME freely selectable. e.g.: 'my_env'
```
virtualenv -p python3 ENVNAME
```
4. activate virtualenv
```
. ENVNAME/bin/activate
```
5. run the setup.py via pip3
```
pip3 install .
```
6. finish, now you can start the 'Konfigurator' by typing 
```
konfigurator
```

Mac OS X
--------

1. install python3, virtualenv
```
brew install python3
```
2. clone the repository (download source)
```
git clone https://gitlab.com/spasscan/konfigurator.git
cd konfigurator
```
3. create virtualenv
```
python3 -m venv ENVNAME
```
4. activate virtualenv
```
source ENVNAME/bin/activate
```
5. run the setup.py via pip3
```
pip3 install .
```
6. finish, now you can start the 'Konfigurator' by typing 
```
konfigurator
```

Other, MS Windows
-----------------

* not supported
* try CygWin, Docker
* Dockerfile comming soon

Synopsis
========

* activate virtualenv 4. above
* now
```
konfigurator
```

Update
======
1. go to the 'Konfigurator' directory
2. type `git pull`
3. active your virtualenv
4. now update your installation: `pip3 install --upgrade .`


useful commands
===============

* sudo tcpdump udp port 4321 -vv -X

display all networktrafic udp, 4321

* echo -e -n "\x00\x00\x01\x00\xff\x08\x04\x07\x53\x81\x08\x27\x11\x15\x0c" | nc -w 1 -u -4 224.0.0.1 4321

simulate spasscan 'Melder16' - programming package


