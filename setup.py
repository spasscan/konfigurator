from setuptools import setup

setup (
    name='konfigurator',
    version='0.1',
    description='simple configurator for spasscan modules',
    license="None",
    author='Henry Becker',
    packages=['spasscan', 'spasscan.communication','spasscan.udp', 'spasscan.mup', 'spasscan.mst'],
    install_requires=['lxml'],
    entry_points={
        'console_scripts' : [
            'konfigurator = spasscan.__main__:main_cli',
            'konfigurator_tk = spasscan.__main__:main_tk'
            ]
        },
)
