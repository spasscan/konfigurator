import sys

def main_cli(args=None):
    """The main routine."""
    from .spass_cli import SpassCanKonfigurator_CLI
    if args is None:
        args = sys.argv[1:]

    spass = SpassCanKonfigurator_CLI( )
    spass.run()
 
def main_tk(args=None):
    """The main routine."""
    from .spass_tk import SpassCanKonfigurator_TK
    if args is None:
        args = sys.argv[1:]

    spass = SpassCanKonfigurator_TK( )
    spass.run()
    

if __name__ == "__main__":
    main()

