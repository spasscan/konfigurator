from .mup.queue import Queue, QueueElement
from .mup.module import Module
import time
import sys

class CLI(object):
    def __init__(self):
        self.command = None

        self.queue = Queue()
        self.track_port_changes = False
        self.debugmode = False

        self.messages = []

        self.mup = Module()
        self.revision = None
        self.popupulate_revision_from_file()

    def popupulate_revision_from_file(self):
        path_file = '.revision'

        try:
            revision_file = open(path_file, 'r')
            revision = revision_file.read()
            self.revision = revision
        except: 
            pass

    def set_mup(self,mup):
        self.mup = mup

    def keypress(self, ascii_code):
        if ascii_code == ord('c'):
            cv = input("CV (hex)? ")
            cv_var = input("Value for CV"+str(cv)+' (hex)? ')
            self.queue.append(QueueElement(cv, cv_var))
            self.display_queue()
        elif ascii_code == ord('q'):
            self.messages.append('Ciao.')
            self.command = 'quit' 
        elif ascii_code == ord('h'):
            self.messages.append(self.help_text())
            self.messages.append(self.mup.help_text())
        elif ascii_code == ord('s'):
            self.command = 'send_queue' 
        elif ascii_code == ord('M'):
            cv_var = input("Value for CV10 (Address) (hex)")
            self.queue.append(QueueElement('10', cv_var))
            self.display_queue()
        elif ascii_code == ord('d'):
            self.queue.clear()
        elif ascii_code == ord('D'):
            self.debugmode = not self.debugmode
            self.messages.append('debug mode (on/off)')
        elif ascii_code == ord('t'):
            self.track_port_changes = not self.track_port_changes
            self.messages.append('tracking port changes (on/off)')
        elif ascii_code == ord('l'):
            self.command = 'list port states'
        else:
            self.messages.clear()
            self.command = None

    def help_text(self):
        return """
      # Common #
<h> - this help text
<q> - quit programm
<c> - set CV Variable
<s> - send queue and terminate programming
<d> - delete queue

<D> - toogle Debugmode
<l> - list all known modules and theirs port states
<t> - tracking port changes
"""

    def flush_messages(self):
        for message in self.messages:
            self.print(message)
        self.messages.clear()

    def display_module_table(self):
        self.print(self.mup.get_display_status()) 

    def display_queue(self):
        self.print("Queue:")
        for queue_element in self.queue.get():
            self.print(queue_element.human_readable())

 
    def debug(self, msg):
        if self.debugmode:
            self.print(msg)

    def print(self, msg):
        msg = msg.replace("\n","\r\n")
        print (msg, "\r")

    def display_startup_screen(self):
        self.print ("")
        self.print(""" _____                          _____               """)
        self.print("""/  ___|                        /  __ \              """)
        self.print("""\ `--.  _ __    __ _  ___  ___ | /  \/  __ _  _ __  """)
        self.print(""" `--. \| '_ \  / _` |/ __|/ __|| |     / _` || '_ \ """)
        self.print("""/\__/ /| |_) || (_| |\__ \\\\__ \| \__/\| (_| || | | |""")
        self.print("""\____/ | .__/  \__,_||___/|___/ \____/ \__,_||_| |_|""")
        self.print("""       | |                                          """)
        self.print("""       |_|                     Konfigurator, rev: """+ str(self.revision))
        self.print ("")
        self.print ("press <h> for help")
 
