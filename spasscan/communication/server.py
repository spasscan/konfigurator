import asyncio
import binascii
import sys
import time
import socket
from collections import deque
from .udppackage import UDP_byte_list

class UDP_protocol:
    # TODO FIXME , use vars from mup
    prog_write = "00 0000 0000 05 01 06" 
    prog_read  = "00 0000 0000 05 02 06" 

    def __init__(self, send_to_addr='224.0.0.1', send_to_port=4321):
        self.udp_byte_list = deque( [] )

        self.send_to_addr = send_to_addr
        self.send_to_port = send_to_port

#isnt in use now
#     def send_prog_read_cv(self, mup, cv):
#         base_to_send = self.prog_read
#         base_to_send += mup.udp_packet.hw_addr
#      
#         ready_to_send = base_to_send + " " + cv
#         ready_to_send_byte = bytearray.fromhex(ready_to_send)
#         self.transport.sendto(ready_to_send_byte, ('224.0.0.1',4321))

    def send_queue(self, module, queue):
        print ("Send")
        self.check_queue = {}

        base_to_send = self.prog_write
        base_to_send += module.id

        terminate_programming = False
        for queue_element in queue.get():
            ready_to_send = base_to_send 
            ready_to_send += str(queue_element.cv) + " "
            ready_to_send += str(queue_element.value)
            ready_to_send_byte = bytearray.fromhex(ready_to_send)
#             print (ready_to_send_byte)
            count = 0
            while count != 10:
                count = count +1
                self.transport.sendto(ready_to_send_byte, (self.send_to_addr,self.send_to_port))
                terminate_programming = True

        if terminate_programming:
            ready_to_send = base_to_send + ' FF FF'
            ready_to_send_byte = bytearray.fromhex(ready_to_send)
            count = 0
            while count != 10:
                count = count +1
                self.transport.sendto(ready_to_send_byte, (self.send_to_addr,self.send_to_port))

        print ("... send done")

    def connection_made(self, transport):
        self.transport = transport
        
#        if transport:
#            self.transport = transport
#        else:
#            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#            sock.bind(('', 4321))
#            group = socket.inet_aton('224.0.0.1')
#            iface = socket.inet_aton('192.168.2.107')
#            #mreq = struct.pack('4sL', group, socket.INADDR_ANY)
#            sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, group+iface)#mreq)
#            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#    #        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)    
#            self.transport = sock
        
    def datagram_received(self, data, addr):
        message = data.hex()
        hexmessage = binascii.hexlify(data)

        byte_list = UDP_byte_list(message)
        if byte_list != None:
            self.udp_byte_list.append(byte_list)

    def error_received(self, exception):
        print('Error received:', exception)
        sys.exit(0)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    print("Starting UDP server")
    # One protocol instance will be created to serve all client requests
    listen = loop.create_datagram_endpoint(
        UDP_protocol, local_addr=('224.0.0.1', 4321)
        )

    transport, protocol = loop.run_until_complete(listen)

    try:
       loop.run_forever()
    except KeyboardInterrupt:
        pass

    transport.close()
    loop.close()
