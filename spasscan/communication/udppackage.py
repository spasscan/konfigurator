
def UDP_byte_list(UDP_string):
    if len(UDP_string) in [30,32,24]:
        package = list(map(''.join, zip(*[iter(UDP_string)]*2)))
        return package
    else:
        return None

