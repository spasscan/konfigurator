
class ModuleDictionary(object):
    def __init__(self):
        self.list = {}
        self.last_change_key = None

    def append(self, module):
        tmp_port_states = dict()
        if module.sender in self.list:
            tmp_port_states = self.list[module.sender]
        tmp_port_states[module.port] = module.state
        self.list[module.sender] = tmp_port_states

    def get_module_port_state_as_string(self, addr, port):
        state = self.list[addr][port]

        rr_addr_dec = str(int(addr+port,16)) # rocrail addr, dec
        addr_dec = str(int(addr,16))
        port_dec = str(int(port, 16))
        state_dec = str(int(state, 16))

        return "Module {}({}), Port {}({}) changed to {}({}), rr: {}".format(addr,addr_dec,port, port_dec, state, state_dec, rr_addr_dec)

    def get_all_ports_as_list(self):
        messages = []
        for module_addr, val in  self.list.items():
            states = []
            for port, state in val.items():
                rr_addr_dec = str(int(module_addr + port, 16))
                states.append(port + '('+rr_addr_dec+') => ' +state )
            module_addr_dec = str(int(module_addr,16))
            messages.append('Module:"'+module_addr+'('+module_addr_dec+') :: '+str.join(' | ',states))
        return messages

