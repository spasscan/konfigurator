from .melder import Melder16, Melder24, Monitor16, S88, RFID
from .module import Module
import sys


"""
    decide by hardware type which kind of module
"""
def Factory(mup):
    module_type = int(mup.hw_type, 16)
    if module_type == 12:
        return Melder16(mup)
    elif module_type == 13:
        return Melder24(mup)
    elif module_type == 14:
        return Monitor16(mup)
    elif module_type == 10:
        return S88(mup)
    elif module_type == 11:
        return RFID(mup)
 
    return Module()
