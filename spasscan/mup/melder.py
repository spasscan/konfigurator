from .module import Module

class Melder(Module):
    def __init__(self, udp_object, name):
        super().__init__(udp_object, name)        

class RFID(Melder):
    def __init__(self, udp_object):
        super().__init__(udp_object, 'RFID')

class Monitor16(Melder):
    def __init__(self, udp_object):
        super().__init__(udp_object, 'Monitor16')

class S88(Melder):
    def __init__(self, udp_object):
        super().__init__(udp_object, 'S88')

class Melder16(Melder):
    def __init__(self, udp_object):
        super().__init__(udp_object, 'Melder16')

    def help_text(self):
        string = super().help_text()
        string += """
      # Specials for Melder16 (not implement yet)
<A> - set mask for Port 1-8
<B> - set mask for Port 9-16
<Z> - set mask for Port 1-8 to FFh and append in queue
<Y> - set mask for Port 9-16 to FFh and append in queue
"""
        return string

class Melder24(Melder):
    def __init__(self, udp_object):
        super().__init__( udp_object, 'Melder24')

    def help_text(self):
        string = super().help_text()
        string += """
      # Specials for Melder24 (not implement yet) 
<A> - set mask for Port 1-8
<B> - set mask for Port 9-16
<C> - set mask for Port 17-24
<Z> - set mask for Port 1-8 to FFh and append in queue
<Y> - set mask for Port 9-16 to FFh and append in queue
<X> - set mask for Port 17-24 to FFh and append in queue
"""
        return string

