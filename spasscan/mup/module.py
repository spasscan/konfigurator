import time
from .queue import Queue

class Module:
    programming_package_frequenz = 15

    def __init__(self, udp_object = None, name = 'UNKNOWN'):
        self.send_queue = Queue()

        self.programming_mode = False
        self.id = None
        self.udp_object = udp_object
        self.name = name
        self.insert_time = (time.time()-self.programming_package_frequenz)
        self.delta_inserttime = 0

        if self.udp_object != None:
            self.id = self.udp_object.hw_addr

    def set_programming_mode(self):
        self.programming_mode = True

    def unset_programming_mode(self):
        self.programming_mode = False

    def check_programming_mode(self):
        delta_inserttime = time.time() - self.insert_time
        self.delta_inserttime = delta_inserttime
        if delta_inserttime > self.programming_package_frequenz and self.programming_mode:
            self.unset_programming_mode()
        if delta_inserttime <= self.programming_package_frequenz and not self.programming_mode:
            self.set_programming_mode()

    def update_time(self):
        self.insert_time = time.time()


    def get_display_status(self):
        string = self.name
        if self.programming_mode:
            string += "<<active>>"
        else:
            string += "<<inactive>>"

        string += """
RocNet                          | SpassCan
Net| Recv | Send |Grp |Act |Len | HW Addr  | HV | SV | HT
"""
        if self.udp_object != None:
            string += self._rocnet_header() + " | " + self._spasscan_payload()
        return string

    def _rocnet_header(self):
        string = str(self.udp_object.net) + ' | '
        string += str(self.udp_object.reciever) + ' | '
        string += str(self.udp_object.sender) + ' | '
        string += str(self.udp_object.group) + ' | '
        string += str(self.udp_object.action) + ' | '
        string += str(self.udp_object.length)
        return string

    def _spasscan_payload(self):
        string = str(self.udp_object.hw_addr) + ' | '
        string += str(self.udp_object.hw_ver) + ' | '
        string += str(self.udp_object.sw_ver) + ' | '
        string += str(self.udp_object.hw_type)
        return string

    def help_text(self):
        return """
      # short hands for every module
<M> - Module Address
"""
