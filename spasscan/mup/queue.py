class Queue(object):
    def __init__(self):
        self.list = []

    def append(self, queue_element):
        if self._valid(queue_element):
            self.list.append(queue_element)
        else:
            print ("invalid Queue Element" + string)

    def clear(self):
        self.list.clear()

    def get(self):
        return self.list

    def _valid(self, queue_element):
        return True

class QueueElement(object):
    def __init__(self, cv, value):
        self.cv = '{:0>2}'.format(cv.strip())
        self.value = '{:0>2}'.format(value.strip())

        
    def packagepart(self):
        return str(self.cv) + str(self.value)

    def human_readable(self):
        return "CV (" + self.cv + "h) " +str(int(self.cv, 16))+" => (" + self.value + "h) " + str(int(self.value, 16))

