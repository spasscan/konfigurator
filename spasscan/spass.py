from .communication.server import UDP_protocol
from .udp.factory import SpasscanFactory
from .mup.module import Module 
from .mup.factory import Factory
from .mup.queue import Queue, QueueElement
from .mst.list import ModuleDictionary
# import time
# import sys
# import signal
import asyncio
from collections import deque


class SpassCanKonfigurator:
    programming_package_frequenz = 5

    def __init__(self):
        self.orig_settings = None
        self.key_input = 0
        self.loop = None

        self.datagram_endpoint = None

        self.mup = Module() 
        self.modules_state = {}
        self.mst_dict = ModuleDictionary()

        self.shutdown = False
        self.command = None

        self.queue = Queue()
        self.track_port_changes = False
        self.debugmode = False

        self.messages = []

        self.mup = Module()
        self.revision = None
        self.popupulate_revision_from_file()

    def popupulate_revision_from_file(self):
        path_file = '.revision'

        try:
            revision_file = open(path_file, 'r')
            revision = revision_file.read()
            self.revision = revision
        except: 
            pass

    def set_mup(self,mup):
        self.mup = mup

    def keypress(self, ascii_code):
        if ascii_code == ord('c'):
            cv = input("CV (hex)? ")
            cv_var = input("Value for CV"+str(cv)+' (hex)? ')
            self.queue.append(QueueElement(cv, cv_var))
            self.display_queue()
        elif ascii_code == ord('q'):
            self.messages.append('Ciao.')
            self.command = 'quit' 
        elif ascii_code == ord('h'):
            self.messages.append(self.help_text())
            self.messages.append(self.mup.help_text())
        elif ascii_code == ord('s'):
            self.command = 'send_queue' 
        elif ascii_code == ord('M'):
            cv_var = input("Value for CV10 (Address) (hex)")
            self.queue.append(QueueElement('10', cv_var))
            self.display_queue()
        elif ascii_code == ord('d'):
            self.queue.clear()
        elif ascii_code == ord('D'):
            self.debugmode = not self.debugmode
            self.messages.append('debug mode (on/off)')
        elif ascii_code == ord('t'):
            self.track_port_changes = not self.track_port_changes
            self.messages.append('tracking port changes (on/off)')
        elif ascii_code == ord('l'):
            self.command = 'list port states'
        else:
            self.messages.clear()
            self.command = None

    def help_text(self):
        return """
      # Common #
<h> - this help text
<q> - quit programm
<c> - set CV Variable
<s> - send queue and terminate programming
<d> - delete queue

<D> - toggle Debugmode
<l> - list all known modules and theirs port states
<t> - tracking port changes
"""

    def flush_messages(self):
        for message in self.messages:
            self.print(message)
        self.messages.clear()

    def display_module_table(self):
        self.print(self.mup.get_display_status()) 

    def display_queue(self):
        self.print("Queue:")
        for queue_element in self.queue.get():
            self.print(queue_element.human_readable())

 
    def debug(self, msg):
        if self.debugmode:
            self.print(msg)

    def print(self, msg):
        msg = msg.replace("\n","\r\n")
        print (msg, "\r")

    def get_startup_screen(self):
        ret = "\r"
        ret += """ _____                          _____               \n"""
        ret += """/  ___|                        /  __ \              \n"""
        ret += """\ `--.  _ __    __ _  ___  ___ | /  \/  __ _  _ __  \n"""
        ret += """ `--. \| '_ \  / _` |/ __|/ __|| |     / _` || '_ \ \n"""
        ret += """/\__/ /| |_) || (_| |\__ \\\\__ \| \__/\| (_| || | | |\n"""
        ret += """\____/ | .__/  \__,_||___/|___/ \____/ \__,_||_| |_|\n"""
        ret += """       | |                                          \n"""
        ret += """       |_|                     Konfigurator, rev: """+ str(self.revision)+"\n"
        ret += "\n"
        ret += "press <h> for help\n"
        
        return ret
 
    def handle_incomming_udp_package(self):
        while len(self.datagram_endpoint.udp_byte_list) > 0:
            incomming_udp_list = self.datagram_endpoint.udp_byte_list.popleft()
            spasscan_object = SpasscanFactory(incomming_udp_list)
            self.debug('incomming udp' + str(incomming_udp_list))
            if spasscan_object.__class__.__name__ == "SpasscanProg":
                self.debug('is a programming package')
                self.handle_mup(spasscan_object)
            elif spasscan_object.__class__.__name__ == "SpasscanState":
                self.debug('is a CAN - Port state package')
                self.handle_port_state(mst=spasscan_object)
            elif spasscan_object.__class__.__name__ == "Rocnet":
                self.debug('is a unknown rocnet package')

    def handle_port_state(self, mst):
        self.mst_dict.append(mst)
        if self.track_port_changes:
            self.messages.append(self.mst_dict.get_module_port_state_as_string(mst.module_addr(), mst.port))

    def handle_mup(self,mup):
        module_candidate = Factory(mup)
        if module_candidate.id != self.mup.id:
            self.mup = module_candidate
            self.mup.set_programming_mode()
            self.set_mup(self.mup)
            self.display_module_table()
            self.update_mup()
        self.mup.update_time()

    def update_mup(self):
        pass

    def handle_command(self, command):
        if command == 'send_queue':
            if self.mup.programming_mode:
                self.datagram_endpoint2.send_queue(self.mup, self.queue)
                self.queue.clear()
                self.set_mup(Module())
            else:
                self.messages.append('Module not in programming mode')
                self.flush_messages()

        elif command == 'list port states':
            self.messages.append('Port states for known modules:')
            self.messages.extend(self.mst_dict.get_all_ports_as_list())
        elif command == 'quit':
            self.reset_tty()
            self.shutdown=True

