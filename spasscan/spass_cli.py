from .spass import SpassCanKonfigurator
from .communication.server import UDP_protocol
from .udp.factory import SpasscanFactory
from .mup.module import Module 
from .mup.factory import Factory
from .mst.list import ModuleDictionary
import time
import sys
import signal
import asyncio
import tty
import termios
import select
from collections import deque


class SpassCanKonfigurator_CLI(SpassCanKonfigurator):
    programming_package_frequenz = 5

    def __init__(self):
        super().__init__()
        

    def update_mup(self):
        pass

    def reset_tty(self):
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.orig_settings)    

    def handle_command(self, command):
        if command == 'quit':
            self.reset_tty()
        super().handle_command(command)

    def isKeyboardData(self):
        return select.select([sys.stdin], [], [], 0.02) == ([sys.stdin], [], [])

    @asyncio.coroutine
    def get_input(self):
        old_settings = termios.tcgetattr(sys.stdin)
        try:
            tty.setcbreak(sys.stdin.fileno())        
            while self.shutdown == False:
                check = yield from self.loop.run_in_executor(None, self.isKeyboardData)
                if check:
                    c = sys.stdin.read(1)
                    self.key_input = ord(c)
        finally:
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)


    async def main_task(self):
        c = 0

        while self.shutdown == False:
            if len(self.datagram_endpoint.udp_byte_list) > 0:
                self.handle_incomming_udp_package()
            
            c = self.key_input
            if c >= 65 and c <= 122:
                self.reset_tty()
                self.keypress(c)
                tty.setcbreak(sys.stdin.fileno())        
            self.key_input = 0

            self.mup.check_programming_mode()

            if self.command != None:
                self.handle_command(self.command)
                self.command = None

            self.flush_messages()

            await asyncio.sleep(0.5)
        
        self.loop.stop()

    def run(self):
        self.print (self.get_startup_screen())

        # set tty in raw mode
        self.orig_settings = termios.tcgetattr(sys.stdin)
        tty.setraw(sys.stdin)

        loop = asyncio.get_event_loop()
        self.loop = loop

        asyncio.ensure_future(self.get_input())

        # One protocol instance will be created to serve all client requests
        listen = loop.create_datagram_endpoint(
            UDP_protocol, local_addr=('224.0.0.1', 4321)
            )

        # and one protocol instance will be created to send all spasscan news
        listen2 = loop.create_datagram_endpoint(
            UDP_protocol, local_addr=('0.0.0.0', 4322)
            )

        transport, protocol   = loop.run_until_complete(listen)
        transport2, protocol2   = loop.run_until_complete(listen2)

        self.datagram_endpoint = protocol
        self.datagram_endpoint2 = protocol2

        commandline_interface = loop.run_until_complete(self.main_task())

        try:
           loop.run_forever()
        except KeyboardInterrupt:
           pass

        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.orig_settings)    
        transport.close()
        loop.close()

