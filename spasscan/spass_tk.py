from .spass import SpassCanKonfigurator
from .communication.server import UDP_protocol
from .mup.queue import Queue, QueueElement
import asyncio
import socket
import struct
import tkinter as tk
from tkinter.scrolledtext import ScrolledText

class SpassCanKonfigurator_TK(SpassCanKonfigurator):

    def __init__(self):
        super().__init__()

        self.tk_root = tk.Tk()
        self.lbl_var = tk.StringVar()

        self.prog_module_addr = None
        self.prog_cv = None
        self.prog_value = None
        self.prog_queue_display = None

        self.mup_attr = ['net', 'reciever', 'sender', 'group','action', 'length', 'hw_addr', 'hw_ver', 'sw_ver', 'hw_type']
        self.state_mup = {}
        for attr in self.mup_attr:
            self.state_mup.update( { attr : tk.StringVar() })

        self.numeric_mode = tk.StringVar()
        self.numeric_mode.set("hex")

        self.spawn_window()

    def spawn_window(self):
        self.tk_root.title("SpassCAN Konfigurator")
        self.main_container()

#         tk.Label(self.tk_root, text="current hardware id:").pack()
#         tk.Label(self.tk_root, textvariable=self.lbl_var).pack()
#         tk.Button(self.tk_root, text="send queue", command=lambda: self.simulate_press_key(ord('s'))).pack()
#         tk.Button(self.tk_root, text="toggle debugmode", command=lambda: self.simulate_press_key(ord('D'))).pack()
#         tk.Button(self.tk_root, text="toggle track port changes", command=lambda: self.simulate_press_key(ord('l'))).pack()
#         cv = tk.StringVar()
#         tk.Entry(self.tk_root, textvariable=cv).pack()
#         tk.Button(self.tk_root, text="add cv10 value to queue", command=lambda: self.add_value_to_queue(cv)).pack()

    def main_container(self):
        top_frame = tk.Frame(self.tk_root, bg='white', width=450, height=50, pady=3, relief='solid', borderwidth=1)
        state_frame = tk.Frame(self.tk_root, bg='white', width=450, height=50, pady=3, relief="solid", borderwidth=2)
        main_frame = tk.Frame(self.tk_root, bg='white', width=450, height=50, pady=3)
        bottom_frame = tk.Frame(self.tk_root, bg='white', width=450, height=50, pady=3, padx=3, relief='solid', borderwidth=1)

        self.bottom_frame(bottom_frame)
        self.top_frame(top_frame)
        self.state_frame(state_frame)
        self.main_frame(main_frame)

        top_frame.grid(row=0, column=0, sticky="ew")
        state_frame.grid(row=1, column=0, sticky="nsew")
        main_frame.grid(row=2, column=0, sticky="ew")
        bottom_frame.grid(row=3, column=0, sticky="ew")


    def bottom_frame(self, frame):
        button_set = tk.Frame(frame, bg='white', relief='solid', borderwidth=1, width=600, height=50, pady=10, padx=3)

        tk.Button(button_set, text="quit", command=lambda: self.simulate_press_key(ord('q'))).grid()
        button_set.pack(anchor="e")

    def top_frame(self, frame):
        num_style_config = tk.Frame(frame, bg='white', height=40, width=200, padx=4, pady=4, relief="solid", borderwidth=2)
        self.num_style_config_frame(num_style_config)

        logo = tk.Frame(frame, bg='white', height=40, width=200, pady=4,padx=4, relief="solid", borderwidth=2)
        self.logo_frame(logo)
        
        num_style_config.grid(row=0, column=0, sticky="s")
        logo.grid(row=0, column=1, sticky="nesw")

    def num_style_config_frame(self, frame):
        num_style_modes = [
                ("hexadecimal", "hex"),
                ("decimal", "dec"),
                ]

        for label,value in num_style_modes:
            b = tk.Radiobutton(frame, text=label, variable=self.numeric_mode, value=value)
            b.pack(anchor='w')

    def state_frame(self, frame):
        tk.Label(frame, text='ModuleType').grid(row=0,column=0, columnspan=6, sticky="w")
        tk.Label(frame, text='Rocnet', bg="white").grid(row=1,column=0,columnspan=6, sticky="w")

        tk.Label(frame, text='Net').grid(row=2,column=0)
        tk.Label(frame, text='Recv').grid(row=2,column=1)
        tk.Label(frame, text='Sender').grid(row=2,column=2)
        tk.Label(frame, text='Group').grid(row=2,column=3)
        tk.Label(frame, text='Action').grid(row=2,column=4)
        tk.Label(frame, text='Length').grid(row=2,column=5)

        tk.Label(frame, textvariable=self.state_mup['net']).grid(row=3,column=0)
        tk.Label(frame, textvariable=self.state_mup['reciever']).grid(row=3,column=1)
        tk.Label(frame, textvariable=self.state_mup['sender']).grid(row=3,column=2)
        tk.Label(frame, textvariable=self.state_mup['group']).grid(row=3,column=3)
        tk.Label(frame, textvariable=self.state_mup['action']).grid(row=3,column=4)
        tk.Label(frame, textvariable=self.state_mup['length']).grid(row=3,column=5)

        tk.Label(frame, text='SpassCAN', bg="white").grid(row=1,column=6,columnspan=4,sticky="w")

        tk.Label(frame, text='HW Addr').grid(row=2,column=6)
        tk.Label(frame, text='HV').grid(row=2,column=7)
        tk.Label(frame, text='SV').grid(row=2,column=8)
        tk.Label(frame, text='HT').grid(row=2,column=9)

        tk.Label(frame, textvariable=self.state_mup['hw_addr']).grid(row=3,column=6)
        tk.Label(frame, textvariable=self.state_mup['hw_ver']).grid(row=3,column=7)
        tk.Label(frame, textvariable=self.state_mup['sw_ver']).grid(row=3,column=8)
        tk.Label(frame, textvariable=self.state_mup['hw_type']).grid(row=3,column=9)

    def main_frame(self, frame):
        programming_frame = tk.Frame(frame, bg='white', height=40, width=300, padx=4, pady=4, relief="solid", borderwidth=1)
        log_frame = tk.Frame(frame, bg='white', height=40, width=300, padx=4, pady=4, relief='solid', borderwidth=1)
        self.programming_frame(programming_frame)
        self.log_frame(log_frame)
        programming_frame.grid(row=0, column=0, sticky="n")
        log_frame.grid(row=0, column=1, sticky="n")

    def programming_frame(self, frame):
        foo = tk.StringVar()

        tk.Label(frame, text='Programming area').grid(row=0,column=0, columnspan=4, sticky="w")

        tk.Label(frame, text='Module Addr.:').grid(row=1,column=0,columnspan=2, sticky="e")
        self.prog_module_addr = tk.Entry(frame, width=7)
        self.prog_module_addr.grid(row=1,column=2, sticky="w")
        tk.Label(frame, textvariable=self.numeric_mode).grid(row=1,column=3, sticky="w")

        tk.Button(frame, text="add to Queue",width=8, command=lambda: self.add_module_addr_to_queue()).grid(row=2,column=2, columnspan=2, sticky="w")

        tk.Label(frame, text='CV:').grid(row=3,column=0,columnspan=2, sticky="e")
        self.prog_cv = tk.Entry(frame, width=4)
        self.prog_cv.grid(row=3,column=2)
        tk.Label(frame, textvariable=self.numeric_mode).grid(row=3,column=3)

        tk.Label(frame, text='Value:').grid(row=4,column=0,columnspan=2, sticky="e")
        self.prog_value = tk.Entry(frame, width=4)
        self.prog_value.grid(row=4,column=2)
        tk.Label(frame, textvariable=self.numeric_mode).grid(row=4,column=3)

        tk.Button(frame, text="add to Queue", width=8, command=lambda: self.add_cv_value_to_queue()).grid(row=5,column=2, columnspan=2)

        tk.Label(frame, text='Queue:').grid(row=6,column=0, columnspan=4, sticky="w")
        self.prog_queue_display = tk.Text(frame, height=5, width=25)
        self.prog_queue_display.grid(row=7, column=0,columnspan=4, sticky="w")

        tk.Button(frame, text="clear queue", command=lambda: self.delete_queue()).grid(row=8,column=0, columnspan=2)
        tk.Button(frame, text="send queue", command=lambda: self.send_queue()).grid(row=8,column=2, columnspan=2)

    def log_frame(self, frame):

        tk.Button(frame, text="toggle debug", command=lambda: self.simulate_press_key(ord('D'))).grid(row=0,column=0)
        tk.Button(frame, text="toggle track", command=lambda: self.simulate_press_key(ord('t'))).grid(row=0,column=1)
        tk.Button(frame, text="show module states", command=lambda: self.simulate_press_key(ord('l'))).grid(row=0,column=2)

        self.log_display = ScrolledText(frame, height=16, width=70)
        self.log_display.grid(row=2, column=0,columnspan=3)

    def logo_frame(self, frame):
        return
        base=self.get_logo_base64()
        logo = tk.PhotoImage(data=base)
        label = tk.Label(frame, image=logo)
        label.image = logo
        label.pack(side="right")

    def add_module_addr_to_queue(self):
        if self.numeric_mode.get() == 'dec':
            self.queue.append(QueueElement('10', hex(int(self.prog_module_addr.get())).split('x')[-1]))
        else:
            self.queue.append(QueueElement('10', self.prog_module_addr.get()))
        self.prog_module_addr.delete(0, tk.END)
        self.display_queue()

    def add_cv_value_to_queue(self):
        self.queue.append(QueueElement(self.prog_cv.get(), self.prog_value.get()))
        self.prog_cv.delete(0, tk.END)
        self.prog_value.delete(0, tk.END)
        self.display_queue()

    def display_queue(self):
        self.prog_queue_display.delete('1.0', tk.END)
        for queue_element in self.queue.get():
            self.prog_queue_display.insert(tk.END, queue_element.human_readable()+"\n")

    def delete_queue(self):
        self.queue.clear()
        self.display_queue()

    def send_queue(self):
        self.simulate_press_key(ord('s'))
        self.display_queue()

    def add_value_to_queue(self, value):
        self.queue.append(QueueElement('10', value.get()))
        self.display_queue()

    def update_mup(self):
        self.print('update mup from gui')
#         attr = 'net'
#         self.state_mup['net'].set(self.mup.udp_object.__dict__[attr])
#         for attr in ('net', 'reciever', 'sender', 'group','action', 'length', 'hw_addr', 'hw_ver', 'sw_ver', 'hw_type')
        for attr in self.mup_attr:
            self.state_mup[attr].set(self.mup.udp_object.__dict__[attr])
        

    def simulate_press_key(self, ascii_code):
        self.keypress(ascii_code)

    def print(self, msg):
        self.log_display.insert(tk.END, msg+"\n")
        self.log_display.see("end")

    @asyncio.coroutine
    def main_window(self):
        try:
            while self.shutdown == False:

                self.tk_root.update()

                if len(self.datagram_endpoint.udp_byte_list) > 0:
                    self.handle_incomming_udp_package()
                
                c = self.key_input
                if c >= 65 and c <= 122:
                    self.keypress(c)
                self.key_input = 0

                self.mup.check_programming_mode()

                if self.command != None:
                    self.handle_command(self.command)
                    self.command = None

                self.flush_messages()

               
                yield from asyncio.sleep(0.05)
        except:
            self.shutdown = True
            self.loop.stop()
#             tkinter.TclError as e:
#             if "application has been destroyed" not in e.args[0]:
#                 raise

    def make_sock(self, iface):
       
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(('', 4321))
        group = socket.inet_aton('224.0.0.1')
        iface = socket.inet_aton(iface)
        #iface = socket.inet_aton('127.0.1.1')
        #mreq = struct.pack('4sL', group, socket.INADDR_ANY)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, group+iface)#mreq)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)    
        return sock


    def run(self):
        list_host = socket.gethostbyname_ex(socket.gethostname())
        use_host_interface = list_host[2][0]
        print(list_host)
        print(list_host[2])
        self.print(self.get_startup_screen())
        self.print('konfigurator started')

        loop = asyncio.get_event_loop()
        self.loop = loop

        # One protocol instance will be created to serve all client requests
        listen = loop.create_datagram_endpoint(lambda: UDP_protocol(), sock=self.make_sock(use_host_interface))
#        listen = loop.create_datagram_endpoint(
#            UDP_protocol, local_addr=('224.0.0.1', 4321)
#            )
        # and one protocol instance will be created to send all spasscan news
        listen2 = loop.create_datagram_endpoint(
            UDP_protocol, local_addr=('0.0.0.0', 4322)
            )

        transport, protocol   = loop.run_until_complete(listen)
        transport2, protocol2   = loop.run_until_complete(listen2)

        self.datagram_endpoint = protocol
        self.datagram_endpoint2 = protocol2

        asyncio.ensure_future(self.main_window())

        try:
           loop.run_forever()
        except KeyboardInterrupt:
           pass

        transport.close()
        loop.close()

    def get_logo_base64(self):
        return 'iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAIAAAC3LO29AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gEKFCcq39Sp4gAADnBJREFUeNrtWnlMVFfbv+feYYYZhqFMwzqChEVkmxcVBOEVSyohRUVpWmhMGhpriWmiLbY1ltambZpiI100aamtkrTSktbE5VVZFCiLLLIqMHTYZqAwMGyzMfvMvef94/hex2EY0Q/Tj4Zf+Gcuzz3n+T3nOc82AyCE2D8a+N+twBrDNYZrDNcY/gOwxnD1Y43h6scaw9UPxt+twJNAqVS2tLSMj49zudzk5OSgoCAnwit2hhBCtVo9PDw8ODg4MzNDkuRTojc5Ofnll1+Oj4+HhYUBAL7++muRSOREfmXOUKfTXblypbu729XVFcdxvV7v4+Ozd+/eDRs2rCw9CGFZWZlQKMzOzkZPeDze5cuXIyMjAQBPiyFJkiUlJUaj8e233/bx8cFxXKPRtLS0/Pjjj4cOHQoJCVlBhjMzM+Pj47m5ufST8PDw2tpak8nk6ur6tBgODg5KJJJPPvmEx+OhJ56enhkZGbGxsW5ubitID8Ow+fl5Lpf7zDPP0E/kcjmXy2UymUu9sgIMZTKZl5cXTY+Gv7+/Q3mSJI1GI4PBYLFYDgUghCaTiSRJV1dXgiBs/2W1WjkcDv1Qq9XeuHEjPT0dx5cMKCvA0MPDY35+3mQyLaUxhmFKpVIkEm3ZsqWtre3OnTs6nQ5CGBAQsHv3bj8/P1sCvb29LS0ts7OzVquVxWJlZGRs3rzZdin6vkEIS0tLfX19t2/f7kS9FWAYGRkJIbx48WJOTo6Li4tDGZVKdeHChbq6OjabnZaWJhAI9Hp9XV3dqVOn3n33Xfq0r1y5IhKJUlJSMjMzmUxmf3//+fPnORzOxo0bF6/Z39/f39//8ccfMxjOWKwAQzc3tyNHjpw5c0YkEmVmZm7atGnxpcdxfGpqavfu3S+88AKt0GuvvVZSUvLbb7/l5+ejJ0lJSRkZGRwOB3309vaenZ29deuWQ4Y9PT0RERF8Pt+5eiuTD4OCgj799NNt27aVlZUVFBRUVFQsLCzYClAUFRUVlZaWZmfv9PT0kZGRubk59NHf35+mhyAUCuVyudVqXbypq6urQqF45KxwxTI+l8vNzMw8efLk3r176+vrCwoKWltbbQUAAItTlre3t5ub29TUlN1zs9k8PT3d19fX3d2t0Wgc1g/JyclTU1MlJSUqlcqJYitctXE4nB07diQlJTU3N587dw7DsMTERGfbMxhsNlur1dJPJiYmbt++LZVKKYry8PCwWq1LnZK3t/f7779fWlp64sSJtLS09PR0h6HuqdSlLi4uO3bsIEny0qVLW7ZsWSr8IFAURZ9tTU1NZWVlXFxcTk6On58fm80eHx//9ttvl3pXIBC89957IpHop59+Gh4ePnz48OK9nmJvERMTo9fr1Wq1Exmj0ajValG0kEgkly5dysvLy8nJCQ4OZrPZGIZBCJ3fNBzHY2JiPvjgg5GRkc7OTgcC/0caEMKlimySJHEcd5KLESsIoUAgwDCsp6cnNDQ0LCzsCdTw9PSMjY0dHBxceYYajaa4uHhkZGTxv5qbm319fekKa2FhQa/X2wqYzebLly8nJyfTxR1FUbYCFEV1d3erVCqHVbXd2ep0Ond395Vn6O7uHhAQcOrUqYsXL8pkMoPBYDQap6enf//99/r6+ldeeQWdIY7jUqn09OnTHR0dOp3ObDZPTk4WFxczGIyMjAy0lFAoHBoa6urqMpvNRqNRIpGcO3eupaXFxcXFlgy6tzqdrqSkpKury2g0ms3mO3fujI6Obt26dbGGYEW+exKLxdevX5fJZKh7MplMAQEBWVlZgYGBSEAqlX7//ff79u1rbGzUarUMBsNsNkdFRe3Zs4fL5dLrNDY2Xr9+nc1mMxgMBoORkJAQFRVVX1+/b98+FELGxsY6OjpefPFFCGFDQ0NNTQ2O4wRBQAizsrKEQuHjMYQQLiwsyGQymUymUCjMZjMAwM3Nzdvb28/Pz8fHxzY7I2G1Wg0h5PF4Hh4eAAAIoUKhsFgsMpns2rVrBw8ehBAajUYWi+Xp6enQqfR6/dTUlFqt5vP5bDaboiiUNpAhUOCh77bJZJLL5XNzc3w+n8PhUBSFdre1muNsQZJkf3//tWvX6urqJBKJWq22WCzIPXAcd3V15fP5oaGh27dvz8rKQh0gAIDH49l1GHK5/M0335RIJOvXr8/MzMzOztbr9c8//3xhYeFS/Q6HwzEYDEePHtVoNOj6kST53HPPffbZZ1wu165sYLFYAIATJ07I5XJa+MiRIwcOHHDGcGpq6uzZs2VlZRMTE7TB0PsQQoqiFhYWNBqNRCKprq4mCIKuKhejqamprq7OYDAAAAiCGBwc1Ol0Op3u9ddfj4yMXOotvV4/MDCgVqvpTcfHx6Ojow8ePLhY2Gw2Dw0NTUxMID1Jkpyfn7cVsI80IpHojTfeKCoqQu8QBGFrNuQk6CQZDAaTyXQyBbJYLOXl5SaTiSAIOt4QBCGXy2tqarClgdanQRCE0Wj85ptvurq6HMrjD8Mu8D7EcGRk5K233qqtrUV7oIcURaGMx2KxuFwuh8PBcZwkSYvFwuPxnAwphoeHm5ub7fYDAJAkWVlZaVeaOweO4xKJ5IsvvnBegjrEAy/V6XSFhYXNzc22bTVFUf7+/ikpKQkJCQEBAVwu12KxzMzMDAwMtLe3AwCWauQxDKutrZXJZDiOoyxHhzQcx+/evXv37l3nnasdCIKoqqoqKSnJz89fauj0CIY3b968evUqfXQQQgaDkZWVlZ+fHxMTs7jL1Ol0CoXC09PT4boLCwuVlZUkSSJ7EQTBZrNRWAcAqFSqqqqqx2KIYZjFYvnuu+/i4+Mf68X7fAwGQ1lZmU6no80DANi/f/+ZM2c2bdrksIl2c3MLCAiwm6PQ6Onp6e7uRvaiKCo0NDQ+Pt7Pzw+dJISwpqZmcdP0CF1xfHJysrCwcHp6+rEZDg0Ntbe32969iIiIY8eO2U61HgtVVVVKpRLZi8FgJMXFeZtN8XFxiCGKq3YN5HJAEERjY2NxcbHDntgZw3v37ikUClv/3rNnj/NpuRNMT09XV1fTx+Xl7S309v7r3A//3rqV+b8WzmAw3Lhxw2KxLGdB27IEQnj+/Plbt249HsOhoSF6Mwghh8Nx3rk6R2tr6+DgIO2iUdHRfOX8+LX/RHp5CQQCFHgAAE1NTRKJ5NEq4nhgYCB9HQAA8/PzJ0+eHBsbWy5DCOHMzIytnbhcrq+v75PRs1qt5eXler0eeQSO40nx8cauDu3oqNvkxKbNmxFDHMdlMtkff/zxyAUBAK+++mpCQgLdphEE0dnZefr06eW4wH0z2/Y1EEImk4ka0CeARCJpbGyky5FnPD23BAUp29spK7bQ2vLvhAS6DbdarRUVFbYjDIegKCowMPD48eNeXl62x/DLL79cvXr1kZnjfkVmWyWipLzMG7IYdXV1dA1FUVR4RISvXqcdGwUEpmhv+9e6dV4+PkhRHMe7urp6e3sfuSaEMDU1NS8vj46FAACtVnvq1CmxWOy8yb5fTPH5fFtjoFz3BPR0Ol1FRcWDQAdAUny89W631WAGOKYdG+MrFUKhkL6KCoWiqqpqOQwxDMvLy0tNTaV9Fcfx/v7+r776ir4RSzLEMCwoKMjOPH19fU/AsK+vr7OzEy0FIXR3d98avkHZdj8rkEazvr0tOTHRtq6orq5eZn7z8vI6fvz4unXrbEcBbW1tdlnAMcPo6Gh3d3fay0mSLC8v12g0j8uwqqpqfn4e7UdRVEhoaABJaoaHAdoHYPOtzXEhIZ58Pu2oYrG4ra1tmesnJiYePnyYyWQuv3G/zzAqKio8PJy2DY7jt2/f/vXXX+0GJ86BJvC2eyfEx2P9IsuCDgMYhmEAYAvDQ74mQ0RkJO2oer2+vLx8mRkcAJCbm7tr167lK3af4bPPPpuVlWWbc0wmU2Fh4dmzZ500AXbj2ra2Ntt7z+ZwtkVHq1pbbBTELFqD+W538rZttuVhQ0PD6OjoMjXm8XjHjh0LCwtbJskHUeill16Ki4uj7zFKrB9++GFubm5paalIJJqenlYqlXNzc1KptL6+vqio6KOPPqL5I8emK1uKogLXrw9hMtTifvBwqFM0NSVERrjzeLSjTkxM1NXVLZMhhmFCofDo0aMcDmc5vvqgpBYIBAUFBYcOHZLL5XRfbzKZKisra2tr+Xw+moVYLBa1Wq1QKDQajUAgOHDgAJpcjI6ONjQ02PbKcXFxLsNDZqXatgkFOKYW/xmKYaFhYV0dHchrLBZLRUXF/v377b6WcYLs7OzW1taff/55WfmQxs6dOz///HN/f3/bkyQIgiTJmZmZP//8s6Oj4969e2NjY1qtFsdxlUollUqRZH19/V9//UW7KJPFSo6N1bS22lsZYGaVGusXJSYkPFACxzs6Oh4rerPZ7HfeeUcoFD7yRx8PMQQAvPzyyz/88MP27dtRI0+7Aer6CYJAIwlkOa1WKxaLMQwzGAzl5eVmsxkJUxTl7y+I8OApe3uwRSaGFKZsbkqKjaXdDAAwNzd38+bNxfo58cPQ0NBjx455eHjYydh9tK8GcBxPTU29cOFCUVFRSkoKanCtDwP1tV5eXjt37kTfXYrF4p6eHjabzWQymUymi4tLXHyc26TMotEQriyc+dAfwXZRD4rD3LmhGza4uLjQr9TX16MhEqqxEFgsFpPJXKoL3bVrV25uLpKh5e26WWfzUrVaPTAw0NvbK5VKZ2dn0byUy+X6+fkFBwdv3LgxJCQETSYnJyd7enropSCEwcHBfhaLbkyKAQclFcABT/gv0eTU3OwsfZGYTObWrVvd3d1VKlVHRwddNgIAoqOj161b51DJubm5rq4u2lchhOHh4bbTo+XOvNGwFR3yY41J/naszFT//zP++b9NXGO4+rHGcPVjjeHqxxrD1Y81hqsfawxXP9YYrn6sMVz9+C/GlDlNs37BmQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOC0wMS0xMFQyMTozNjoxNyswMTowMIrLYM8AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTgtMDEtMTBUMjE6MzY6MTcrMDE6MDD7lthzAAAAAElFTkSuQmCC'
