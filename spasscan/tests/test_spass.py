from unittest import TestCase
from mock import MagicMock
import asyncio
import time

from spasscan.spass import SpassCanKonfigurator


class SpassCanKonfiguratorTest(TestCase):
    def setUp(self):
        self.konfigurator = SpassCanKonfigurator()
        self.konfigurator.get_input = MagicMock(side_effect=self.my_get_input)

    @asyncio.coroutine
    def my_get_input(self):
        self.konfigurator.key_input=104
        time.sleep(10)
        self.konfigurator.key_input=113

    def test_run(self):
        self.konfigurator.run()
        self.assertEqual(self.konfigurator.shutdown, True)

