from .rocnet import Rocnet
from .prog import SpasscanProg
from .cv import SpasscanCV
from .state import SpasscanState

def SpasscanFactory(UDP_list):
    if len(UDP_list) == 28: # cv abfrage
        return SpasscanCV(UDP_list)

    elif len(UDP_list) == 15: # module prog mode
        return SpasscanProg(UDP_list)

    elif len(UDP_list) == 12:
        return SpasscanState(UDP_list)

    else:
        return Rocnet(UDP_list)
