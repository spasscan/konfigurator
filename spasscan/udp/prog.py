from .rocnet import Rocnet

class SpasscanProg(Rocnet):
    def __init__ (self, package):
        super(SpasscanProg, self).__init__(package)
        self.hw_addr = package[8]+package[9]+package[10]+package[11]
        self.hw_ver = package[12]
        self.sw_ver = package[13]
        self.hw_type = package[14]

