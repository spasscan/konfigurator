
class Rocnet(object):
    """
    >>> obj = Rocnet(['00','00','01','00','ff','08','01','04','00','00','01','01'])
    >>> obj.net
    '00'
    >>> obj.reciever
    '0001'
    >>> obj.sender
    '00ff'
    >>> obj.group
    '08'
    >>> obj.action
    '01'
    >>> obj.length
    '04'
    """
    def __init__ (self, package):
        self.net = package[0]
        self.reciever = ''.join(package[1:3])
        self.sender = ''.join(package[3:5])
        self.group = package[5]
        self.action = package[6]
        self.length = package[7]

if __name__ == '__main__':
    import doctest
    doctest.testmod()
