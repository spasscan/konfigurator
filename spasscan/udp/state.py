from .rocnet import Rocnet

class SpasscanState(Rocnet):
    def __init__(self, package):
        super(SpasscanState, self).__init__(package)
        foo1 = package[8] # now '0'
        foo2 = package[9] # now '0'
        self.state = package[10]
        self.port = package[11]

    def module_addr(self):
        return self.sender
