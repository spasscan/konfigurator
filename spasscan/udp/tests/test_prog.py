from unittest import TestCase

from spasscan.udp.prog import SpasscanProg

class RocnetTest(TestCase):
    def setUp(self):
        self.prog = SpasscanProg(['00','00','01','00','ff','08','01','07','54','81','08','27','11','15','0c'])

    def test_net(self):
        self.assertEqual(self.prog.net,'00')

    def test_reciever(self):
        self.assertEqual(self.prog.reciever,'0001')

    def test_hw_addr(self):
        self.assertEqual(self.prog.hw_addr,'54810827')

