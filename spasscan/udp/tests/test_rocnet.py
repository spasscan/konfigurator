from unittest import TestCase

from spasscan.udp.rocnet import Rocnet

class RocnetTest(TestCase):
    def setUp(self):
        self.rocnet = Rocnet(['00','00','01','00','ff','08','01','04','00','00','01','01'])

    def test_net(self):
        self.assertEqual(self.rocnet.net,'00')

    def test_reciever(self):
        self.assertEqual(self.rocnet.reciever,'0001')

